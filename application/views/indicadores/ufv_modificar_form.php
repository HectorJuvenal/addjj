<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Consultora Contable
        <small>Asesora</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Inicio</a></li>
        <li class="active">Asesora</li>
      </ol>
      
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-sm-offset-3 col-xs-6">
          <div class="box box-info">

            <?php
            foreach ($ufv->result() as $row) {
            ?>
            <?php echo form_open_multipart('ufv/modificardb'); ?>

            <div class="box-header with-border">
              <h3 class="box-title">Modificar Ufv</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">

                <input type="hidden" name="idufv" value="<?php echo $row->idUfv; ?>"></input>

                <div class="form-group">
                  <label class="col-sm-3 control-label">Fecha Ufv:</label>

                  <div class="col-sm-8 input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" name="fechaUfv" class="form-control pull-right" value="<?php echo $row->fechaUfv; ?>" id="datepicker" disabled="disabled">
                  </div>
                  <!-- /.input group -->
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">Ufv</label>
                  <div class="col-sm-9">
                    <input type="text" name="ufv" class="form-control" value="<?php echo $row->ufv; ?>" placeholder="Ufv" >
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button><a href="<?php echo base_url(); ?>index.php/ufv/index">Cancelar</a></button>
                <button type="submit" class="btn btn-info pull-right">Guardar</button>
                <?php echo validation_errors(); ?>
              </div>
              <!-- /.box-footer -->
            </form>

            <?php echo form_close(); ?>
            <?php
            }
            ?>

          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>