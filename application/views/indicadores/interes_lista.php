<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Consultora Contable
        <small>Asesora</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Inicio</a></li>
        <li class="active">Asesora</li>
      </ol>
      
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header col-xs-10">
              <h3 class="box-title">Tasas de Interes</h3>
              
            </div>

            <div class="col-xs-2">
              <?php echo form_open_multipart('interes/agregar'); ?>
                <button type="submit" class="btn btn-block btn-success">Agregar</button>
              <?php echo form_close(); ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Fecha Inicial</th>
                    <th>Fecha Final</th>
                    <th>Tasa</th>
                    <th>Fecha Actualización</th>
                    <th>Usuario</th>
                    <th>Modificar</th>
                    <th>Eliminar</th>
                  </tr>
                </thead>
                <tbody>

                <?php
                $indice=1;
                foreach ($interes->result() as $row) {
                  if ($row->estado==1)
                  {
                  ?>
                  <tr>
                    <td><?php echo $indice; ?></td>
                    <td><?php echo $row->fechainicial; ?></td>
                    <td><?php echo $row->fechafinal; ?></td>
                    <td><?php echo $row->tasa; ?></td>
                    <td><?php echo $row->fechaActualizacion; ?></td>
                    <td><?php echo $row->idUsuario; ?></td>
                    <td>
                      <?php echo form_open_multipart('interes/modificar'); ?>
                        <input type="hidden" name="idinteres" value="<?php echo $row->idInteres; ?>"></input>
                        <button type="submit" class="btn btn-primary">Modificar</button>
                      <?php echo form_close(); ?>
                    </td>
                    <td>
                      <?php echo form_open_multipart('interes/eliminardb'); ?>
                        <input type="hidden" name="idinteres" value="<?php echo $row->idInteres; ?>"></input>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                      <?php echo form_close(); ?>
                    </td>
                  </tr>
                  <?php
                  $indice++;
                  }
                }
                ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No.</th>
                  <th>Fecha Inicial</th>
                  <th>Fecha Final</th>
                  <th>Tasa</th>
                  <th>Fecha Actualización</th>
                  <th>Usuario</th>
                  <th>Modificar</th>
                  <th>Eliminar</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>