<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Consultora Contable
        <small>Asesora</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Inicio</a></li>
        <li class="active">Asesora</li>
      </ol>
      
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-sm-offset-2 col-xs-8">
          <div class="box box-info">

            <?php
            foreach ($multa->result() as $row) {
            ?>
            <?php echo form_open_multipart('multa/modificardb'); ?>

            <div class="box-header with-border">
              <h3 class="box-title">Modificar Multa</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">

                <input type="hidden" name="idmulta" value="<?php echo $row->idMulta; ?>"></input>

                <div class="form-group">
                  <label class="col-sm-4 control-label">Incumplimiento</label>
                  <div class="col-sm-8">
                    <textarea class="form-control" rows="3" type="text" name="incumplimiento" placeholder="Incumplimiento"><?php echo $row->incumplimiento; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Multa Persona Natural</label>
                  <div class="col-sm-8">
                    <input type="text" name="personanatural" class="form-control" value="<?php echo $row->personaNatural; ?>" placeholder="Multa Persona Natural" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Multa Persona Juridica</label>
                  <div class="col-sm-8">
                    <input type="text" name="personajuridica" class="form-control" value="<?php echo $row->personaJuridica; ?>" placeholder="Multa Persona Juridica" >
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button><a href="<?php echo base_url(); ?>index.php/multa/index">Cancelar</a></button>
                <button type="submit" class="btn btn-info pull-right">Guardar</button>
                <?php echo validation_errors(); ?>
              </div>
              <!-- /.box-footer -->
            </form>

            <?php echo form_close(); ?>
            <?php
            }
            ?>

          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>