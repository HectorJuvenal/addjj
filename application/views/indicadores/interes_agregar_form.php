<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Consultora Contable
        <small>Asesora</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Inicio</a></li>
        <li class="active">Asesora</li>
      </ol>
      
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-sm-offset-3 col-xs-6">
          <div class="box box-info">

            <?php echo form_open_multipart('interes/agregardb'); ?>

            <div class="box-header with-border">
              <h3 class="box-title">Agregar Interes</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">

                <div class="form-group">
                  <label class="col-sm-3 control-label">Fecha Inicial:</label>
                  <div class="col-sm-8 input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" name="fechainicial" class="form-control pull-right" id="datepicker">
                  </div>
                  <!-- /.input group -->
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Fecha Final:</label>
                  <div class="col-sm-8 input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" name="fechafinal" class="form-control pull-right" id="datepicker">
                  </div>
                  <!-- /.input group -->
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">Tasa</label>
                  <div class="col-sm-9">
                    <input type="text" name="tasa" class="form-control" placeholder="Tasa" >
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button><a href="<?php echo base_url(); ?>index.php/interes/index">Cancelar</a></button>
                <button type="submit" class="btn btn-info pull-right">Guardar</button>
                <?php echo validation_errors(); ?>
              </div>
              <!-- /.box-footer -->
            </form>

            <?php echo form_close(); ?>

          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>