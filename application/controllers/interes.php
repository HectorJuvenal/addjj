<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interes extends CI_Controller {

	public function index()
	{
		$this->load->view('common/header');
		$data['interes']=$this->interes_model->retornarInteres();
		$this->load->view('indicadores/interes_lista',$data);
		$this->load->view('common/footer');
	}

	public function modificar()
	{
		$idinteres=$_POST['idinteres'];
		$data['interes']=$this->interes_model->recuperarInteres($idinteres);
		$this->load->view('common/header');
		$this->load->view('indicadores/interes_modificar_form',$data);
		$this->load->view('common/footer');
	}

	public function modificardb()
	{
		$idinteres=$_POST['idinteres'];
		$fechainicial=$_POST['fechainicial'];
		$fechafinal=$_POST['fechafinal'];
		$tasa=$_POST['tasa'];

		$fechainicial1=date('Y-m-d', strtotime($fechainicial));
		$fechafinal1=date('Y-m-d', strtotime($fechafinal));
		
		$data['fechainicial']=$fechainicial1;
		$data['fechafinal']=$fechafinal1;
		$data['tasa']=$tasa;

		$this->interes_model->modificarInteres($idinteres,$data);
		redirect('interes/index','refresh'); //refresh->obliga a refrescar cache
	}

	public function agregar()
	{
		$this->load->view('common/header');
		$this->load->view('indicadores/interes_agregar_form');
		$this->load->view('common/footer');
	}

	public function agregardb()
	{
		$fechainicial=$_POST['fechainicial'];
		$fechafinal=$_POST['fechafinal'];
		$tasa=$_POST['tasa'];

		$fechainicial1=date('Y-m-d', strtotime($fechainicial));
		$fechafinal1=date('Y-m-d', strtotime($fechafinal));
		
		$data['fechainicial']=$fechainicial1;
		$data['fechafinal']=$fechafinal1;
		$data['tasa']=$tasa;

		$this->interes_model->agregarInteres($data);
		redirect('interes/index','refresh'); //refresh->obliga a refrescar cache
	}

	public function eliminardb()
	{
		$idinteres=$_POST['idinteres'];
		$estado=0;
		$data['estado']=$estado;
		$this->interes_model->modificarInteres($idinteres,$data);
		redirect('interes/index','refresh'); //refresh->obliga a refrescar cache
	}
}
