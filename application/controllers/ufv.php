<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ufv extends CI_Controller {

	public function index()
	{
		$this->load->view('common/header');
		$data['ufv']=$this->ufv_model->retornarUfv();
		$this->load->view('indicadores/ufv_lista',$data);
		$this->load->view('common/footer');
	}

	public function modificar()
	{
		$idufv=$_POST['idufv'];
		$data['ufv']=$this->ufv_model->recuperarUfv($idufv);
		$this->load->view('common/header');
		$this->load->view('indicadores/ufv_modificar_form',$data);
		$this->load->view('common/footer');
	}

	public function modificardb()
	{
		$idufv=$_POST['idufv'];
		$ufv=$_POST['ufv'];
		$data['ufv']=$ufv;

		$config = array(
        array(
                'field' => 'ufv',
                'label' => 'Importe de Ufv',
                'rules' => 'required|decimal|is_unique[ufv.ufv]',
                'errors' => array(
                	'required' => 'La ufv es un dato requerido',
                	'decimal' => 'La ufv debe ser un numero decimal',
                	'is_unique' => 'La ufv ya existe, no puede repetirse',
        		),
			),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE)
        {
            $data['ufv']=$this->ufv_model->recuperarUfv($idufv);
			$this->load->view('common/header');
			$this->load->view('indicadores/ufv_modificar_form',$data);
			$this->load->view('common/footer');
        }
        else
        {
            $this->ufv_model->modificarUfv($idufv,$data);
			redirect('ufv/index','refresh'); //refresh->obliga a refrescar cache
        }

	}

	public function agregar()
	{
		$this->load->view('common/header');
		$this->load->view('indicadores/ufv_agregar_form');
		$this->load->view('common/footer');
	}

	public function agregardb()
	{
		$fechaufv=$_POST['fechaufv'];
		$ufv=$_POST['ufv'];

		$fechaufv1=date('Y-m-d', strtotime($fechaufv));

		$data['fechaufv']=$fechaufv1;
		$data['ufv']=$ufv;

		$config = array(
        array(
                'field' => 'fechaufv',
                'label' => 'Fecha de Ufv',
                'rules' => 'required|is_unique[ufv.fechaUfv]',
                'errors' => array(
                	'required' => 'La fecha es requerida',
                	'is_unique' => 'La fecha ya ha sido creada, no puede repetirse',
        		),
        	),
        array(
                'field' => 'ufv',
                'label' => 'Importe de Ufv',
                'rules' => 'required|decimal|is_unique[ufv.ufv]',
                'errors' => array(
                	'required' => 'La ufv es un dato requerido',
                	'decimal' => 'La ufv debe ser un numero decimal',
                	'is_unique' => 'La ufv ya existe, no puede repetirse',
        		),
			),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('common/header');
			$this->load->view('indicadores/ufv_agregar_form');
			$this->load->view('common/footer');
        }
        else
        {
            $this->ufv_model->agregarUfv($data);
			redirect('ufv/index','refresh'); //refresh->obliga a refrescar cache
        }

	}

	public function eliminardb()
	{
		$idufv=$_POST['idufv'];
		$estado=0;
		$data['estado']=$estado;
		$this->ufv_model->modificarUfv($idufv,$data);
		redirect('ufv/index','refresh'); //refresh->obliga a refrescar cache
	}
}
