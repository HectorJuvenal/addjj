<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Multa extends CI_Controller {

	public function index()
	{
		$this->load->view('common/header');
		$data['multa']=$this->multa_model->retornarMulta();
		$this->load->view('indicadores/multa_lista',$data);
		$this->load->view('common/footer');
	}

	public function modificar()
	{
		$idmulta=$_POST['idmulta'];
		$data['multa']=$this->multa_model->recuperarMulta($idmulta);
		$this->load->view('common/header');
		$this->load->view('indicadores/multa_modificar_form',$data);
		$this->load->view('common/footer');
	}

	public function modificardb()
	{
		$idmulta=$_POST['idmulta'];
		$incumplimiento=$_POST['incumplimiento'];
		$personanatural=$_POST['personanatural'];
		$personajuridica=$_POST['personajuridica'];
		$data['incumplimiento']=$incumplimiento;
		$data['personanatural']=$personanatural;
		$data['personajuridica']=$personajuridica;

		$config = array(
        array(
                'field' => 'personanatural',
                'label' => 'Multa persona natural',
                'rules' => 'required|is_natural',
                'errors' => array(
                	'required' => 'La multa de persona natural es requerida',
                	'is_natural' => 'La multa de persona natural debe ser entero y positivo',
        		),
        	),
        array(
                'field' => 'personajuridica',
                'label' => 'Multa persona juridica',
                'rules' => 'required|is_natural',
                'errors' => array(
                	'required' => 'La multa de persona juridica es requerida',
                	'is_natural' => 'La multa de persona juridica debe ser entero y positivo',
        		),
			),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE)
        {
            $data['multa']=$this->multa_model->recuperarMulta($idmulta);
			$this->load->view('common/header');
			$this->load->view('indicadores/multa_modificar_form',$data);
			$this->load->view('common/footer');
        }
        else
        {
            $this->multa_model->modificarMulta($idmulta,$data);
			redirect('multa/index','refresh'); //refresh->obliga a refrescar cache
        }
		
	}

	public function agregar()
	{
		$this->load->view('common/header');
		$this->load->view('indicadores/multa_agregar_form');
		$this->load->view('common/footer');
	}

	public function agregardb()
	{
		$incumplimiento=$_POST['incumplimiento'];
		$personanatural=$_POST['personanatural'];
		$personajuridica=$_POST['personajuridica'];
		$data['incumplimiento']=$incumplimiento;
		$data['personanatural']=$personanatural;
		$data['personajuridica']=$personajuridica;


		$config = array(
        array(
                'field' => 'personanatural',
                'label' => 'Multa persona natural',
                'rules' => 'required|is_natural',
                'errors' => array(
                	'required' => 'La multa de persona natural es requerida',
                	'is_natural' => 'La multa de persona natural debe ser entero y positivo',
        		),
        	),
        array(
                'field' => 'personajuridica',
                'label' => 'Multa persona juridica',
                'rules' => 'required|is_natural',
                'errors' => array(
                	'required' => 'La multa de persona juridica es requerida',
                	'is_natural' => 'La multa de persona juridica debe ser entero y positivo',
        		),
			),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('common/header');
			$this->load->view('indicadores/multa_agregar_form');
			$this->load->view('common/footer');
        }
        else
        {
            $this->multa_model->agregarMulta($data);
			redirect('multa/index','refresh'); //refresh->obliga a refrescar cache
        }



		
	}

	public function eliminardb()
	{
		$idmulta=$_POST['idmulta'];
		$estado=0;
		$data['estado']=$estado;
		$this->multa_model->modificarMulta($idmulta,$data);
		redirect('multa/index','refresh'); //refresh->obliga a refrescar cache
	}
}
