<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ufv_model extends CI_Model {

	public function retornarUfv()
	{
		$this->db->select('*');
		$this->db->from('ufv');
		$this->db->order_by('fechaUfv','asc');
		return $this->db->get();
	}

	public function recuperarUfv($idufv)
	{
		$this->db->select('*');
		$this->db->from('ufv');
		$this->db->where('idUfv',$idufv);
		return $this->db->get();
	}

	public function modificarUfv($idufv,$data)
	{
		$this->db->where('idUfv',$idufv);
		$this->db->update('ufv',$data);
	}

	public function agregarUfv($data)
	{
		$this->db->insert('ufv',$data);
	}

	public function eliminarUfv($idufv)
	{
		$this->db->where('idUfv',$idufv);
		$this->db->delete('ufv');
	}

}
