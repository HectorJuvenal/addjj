<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Multa_model extends CI_Model {

	public function retornarMulta()
	{
		$this->db->select('*');
		$this->db->from('multa');
		$this->db->order_by('incumplimiento','asc');
		return $this->db->get();
	}

	public function recuperarMulta($idmulta)
	{
		$this->db->select('*');
		$this->db->from('multa');
		$this->db->where('idMulta',$idmulta);
		return $this->db->get();
	}

	public function modificarMulta($idmulta,$data)
	{
		$this->db->where('idMulta',$idmulta);
		$this->db->update('multa',$data);
	}

	public function agregarMulta($data)
	{
		$this->db->insert('multa',$data);
	}

	public function eliminarMulta($idmulta)
	{
		$this->db->where('idMulta',$idmulta);
		$this->db->delete('multa');
	}

}
