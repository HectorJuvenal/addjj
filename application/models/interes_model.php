<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interes_model extends CI_Model {

	public function retornarInteres()
	{
		$this->db->select('*');
		$this->db->from('interes');
		$this->db->order_by('fechainicial','asc');
		return $this->db->get();
	}

	public function recuperarInteres($idinteres)
	{
		$this->db->select('*');
		$this->db->from('interes');
		$this->db->where('idInteres',$idinteres);
		return $this->db->get();
	}

	public function modificarInteres($idinteres,$data)
	{
		$this->db->where('idInteres',$idinteres);
		$this->db->update('interes',$data);
	}

	public function agregarInteres($data)
	{
		$this->db->insert('interes',$data);
	}

	public function eliminarInteres($idinteres)
	{
		$this->db->where('idInteres',$idinteres);
		$this->db->delete('interes');
	}

}
